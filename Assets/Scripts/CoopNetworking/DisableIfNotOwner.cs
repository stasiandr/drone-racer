using Unity.Netcode;

namespace CoopNetworking
{
    public class DisableIfNotOwner : NetworkBehaviour
    {
        public override void OnNetworkSpawn()
        {
            if (!IsOwner) gameObject.SetActive(false);
        }
    }
}