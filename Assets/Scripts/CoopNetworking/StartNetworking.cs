using System;
using Unity.Netcode;
using UnityEngine;

namespace CoopNetworking
{
    public class StartNetworking : MonoBehaviour
    {
        private void Start()
        {
            if (!ParrelSync.ClonesManager.IsClone())
                GetComponentInParent<NetworkManager>().StartHost();
            else
                GetComponentInParent<NetworkManager>().StartClient();
        }
    }
}