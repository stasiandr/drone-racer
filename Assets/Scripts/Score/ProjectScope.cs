using VContainer;
using VContainer.Unity;

namespace Score
{
    public class ProjectScope : LifetimeScope
    {
        protected override void Configure(IContainerBuilder builder)
        {
            builder.Register<ScoreStorage>(Lifetime.Singleton).AsImplementedInterfaces();
        }
    }
}