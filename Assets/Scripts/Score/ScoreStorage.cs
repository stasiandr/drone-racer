using System.Collections.Generic;
using Interfaces;

namespace Score
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class ScoreStorage : IScoreStorage
    {
        private readonly Dictionary<Teams, int> _teamPoints;

        public ScoreStorage()
        {
            _teamPoints = new();
        }

        void IScoreStorage.AddPoints(Teams team, int points)
        {
            _teamPoints[team] = _teamPoints.GetValueOrDefault(team) + points;
        }

        IReadOnlyDictionary<Teams, int> IScoreStorage.TeamPoints => _teamPoints;
    }
}