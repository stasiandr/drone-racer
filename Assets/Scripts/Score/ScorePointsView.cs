using System;
using System.Collections.Generic;
using AnySerialize;
using Interfaces;
using TMPro;
using UnityEngine;
using VContainer;

namespace Score
{
    public class ScorePointsView : MonoBehaviour
    {
        private IScoreStorage _scoreStorage;

        [AnySerialize] public Dictionary<Teams, TMP_Text> texts { get; }

        [Inject]
        public void Construct(IScoreStorage scoreStorage)
        {
            _scoreStorage = scoreStorage;
        }

        private void Update()
        {
            foreach (var (team, display) in texts)
            {
                display.text = _scoreStorage.TeamPoints.TryGetValue(team, out var points) ? points.ToString() : 0.ToString();
            }
        }
    }
}