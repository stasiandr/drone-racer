using Unity.Netcode;

namespace Interfaces
{
    public interface IOwner
    {
        NetworkVariable<Teams> Team { get; }
    }
}