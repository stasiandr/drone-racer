using System.Collections.Generic;
using AnySerialize;
using Interfaces;
using UnityEngine;
using VContainer;

namespace Rings
{
    public class TeamView : MonoBehaviour
    {
        [AnySerialize] public Dictionary<Teams, GameObject> Rings { get; }

        [Inject]
        public void Construct(IOwner ring)
        {
            ;
            ring.Team.OnValueChanged += (value, newValue) =>
            {
                Rings[value].SetActive(false);
                Rings[newValue].SetActive(true);
            };
        }
    }
}