using System.Collections.Generic;

namespace Interfaces
{
    public interface IScoreStorage
    {
        IReadOnlyDictionary<Teams, int> TeamPoints { get; }
        void AddPoints(Teams team, int points = 1);
    }
}