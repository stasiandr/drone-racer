namespace Interfaces
{
    public enum Teams
    {
        None = 0,
        Blue = 1,
        Orange = 2,
        Green = 4,
    }
}