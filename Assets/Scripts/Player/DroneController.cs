using Interface;
using Nrjwolf.Tools.AttachAttributes;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.InputSystem;
using VContainer;

namespace Player
{
    public class DroneController : NetworkBehaviour
    {
        [GetComponent]
        [SerializeField]
        private Rigidbody rb;

        [field: SerializeField]
        public float Speed { get; private set; }
        
        [field: SerializeField]
        public float RotationSpeed { get; private set; }

        [field: SerializeField]
        public float UpDownSpeed { get; private set; }
        
        [field: SerializeField]
        public float BoosterInitialCapacity { get; set; }
        
        [field: SerializeField]
        private float BoosterUseSpeed { get; set; }
        
        [field: SerializeField]
        public float BoosterSpeedArg { get; set; }

        private InputAction _move;
        private InputAction _upDown;
        private InputAction _enableBoost;
        private float _boosterCapacity;


        public bool BoostEnabled { get; private set; }

        public float BoosterCapacity
        {
            get => _boosterCapacity;
            set => _boosterCapacity = Mathf.Clamp(value, 0f, BoosterInitialCapacity);
        }

        private float CurrentSpeed => BoostEnabled ? Speed * BoosterSpeedArg : Speed;

        [Inject]
        public void Construct(DroneInput droneInput)
        {
            _move = droneInput.Player.Move;
            _upDown = droneInput.Player.UpDown;
            _enableBoost = droneInput.Player.EnableBoost;
        }

        public override void OnNetworkSpawn()
        {
            if (IsOwner) return;
            
            enabled = false;
        }

        private void Awake()
        {
            BoosterCapacity = BoosterInitialCapacity;
        }

        private void Update()
        {
            if (BoostEnabled && BoosterCapacity <= 0)
            {
                BoostEnabled = false;
                return;
            }

            float boosterCapacityChange = BoosterUseSpeed * Time.deltaTime;
            BoosterCapacity += BoostEnabled ? -boosterCapacityChange : boosterCapacityChange;

            if (_enableBoost.WasPerformedThisFrame()) BoostEnabled = !BoostEnabled;
        }

        private void FixedUpdate()
        {
            Move(_move.ReadValue<Vector2>());
            UpDown(_upDown.ReadValue<float>());
        }

        private void Move(Vector2 move)
        {
            rb.AddTorque(Vector3.up * (move.x * RotationSpeed),  ForceMode.Force);
            rb.AddForce(transform.forward * (move.y * CurrentSpeed), ForceMode.Force);
        }

        private void UpDown(float value)
        {
            rb.AddForce(Vector3.up * (UpDownSpeed * value), ForceMode.Force);
        }
    }
}