using System;
using System.Collections.Generic;
using Interfaces;
using Unity.Netcode;
using UnityEngine;

namespace Player
{
    public class PlayerData : NetworkBehaviour, IOwner
    {
        public NetworkVariable<ulong> playerId;
        public NetworkVariable<Teams> team;

        public override void OnNetworkSpawn()
        {
            if (!IsOwner) return;

            playerId = new NetworkVariable<ulong>(0, writePerm: NetworkVariableWritePermission.Owner);
            InitializeServerRpc();
        }

        [ServerRpc]
        public void InitializeServerRpc(ServerRpcParams serverRpcParams = default)
        {
            playerId.Value = serverRpcParams.Receive.SenderClientId;
            
            foreach (var t in new [] {Teams.Blue, Teams.Green, Teams.Orange})
            {
                if (SelectedTeams.Contains(t)) continue;

                team.Value = t;
                SelectedTeams.Add(t);
                break;
            }
            
        }

        public static HashSet<Teams> SelectedTeams = new();

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
        public static void ResetSelectedTeams()
        {
            SelectedTeams = new HashSet<Teams>();
        }

        NetworkVariable<Teams> IOwner.Team => team;
    }
}