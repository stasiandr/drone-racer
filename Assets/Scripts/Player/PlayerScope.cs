using Interface;
using UnityEngine;
using VContainer;
using VContainer.Unity;

namespace Player
{
    public class PlayerScope : LifetimeScope
    {
        [SerializeField] private DroneController droneController;
        private DroneInput _droneInput;

        [SerializeField] 
        private PlayerData playerData;

        protected override void Configure(IContainerBuilder builder)
        {
            _droneInput = new DroneInput();
            builder.RegisterInstance(_droneInput);
            builder.RegisterInstance(droneController);
            builder.RegisterInstance(playerData).AsImplementedInterfaces();
        }
        
        private void OnEnable() => _droneInput.Enable();

        private void OnDisable() => _droneInput.Disable();
    }
}