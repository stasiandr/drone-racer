using System;
using System.Collections;
using Interfaces;
using Player;
using Unity.Netcode;
using UnityEngine;
using VContainer;

namespace Rings
{
    public class CaptureableRing : NetworkBehaviour, IOwner
    {
        public NetworkVariable<Teams> owner;
        private IScoreStorage _scoreStorage;

        public override void OnNetworkSpawn()
        {
            if (!IsOwner) return;
            
            owner.Value = default;

            StartCoroutine(UpdatePoints());
        }

        private IEnumerator UpdatePoints()
        {
            while (true)
            {
                if (owner.Value != Teams.None)
                    AddPointsServerRpc(owner.Value);

                yield return new WaitForSeconds(1);
            }
        }

        [ServerRpc]
        private void AddPointsServerRpc(Teams player)
        {
            AddPointsClientRpc(player);
        }

        [ClientRpc]
        private void AddPointsClientRpc(Teams player)
        {
            _scoreStorage.AddPoints(player);
        }


        [Inject]
        public void Construct(IScoreStorage scoreStorage)
        {
            _scoreStorage = scoreStorage;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!other.attachedRigidbody.TryGetComponent<PlayerData>(out var player)) return;
            if (!IsOwner) return;

            owner.Value = player.team.Value;
        }
        

        NetworkVariable<Teams> IOwner.Team => owner;
    }
}