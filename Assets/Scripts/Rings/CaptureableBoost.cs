﻿using Player;
using UnityEngine;

namespace Rings
{
    public class CaptureableBoost : MonoBehaviour
    {
        [SerializeField] private float boosterCapacityChange;

        private void OnTriggerEnter(Collider other)
        {
            if (!other.TryGetComponentInParent(out DroneController droneController)) return;

            droneController.BoosterCapacity += boosterCapacityChange;
        }
    }

    public static class ComponentExtensions
    {
        public static bool TryGetComponentInParent<T>(this Component self, out T component) where T : Component
        {
            component = null;
            
            while (self != null)
            {
                if (self.TryGetComponent(out component)) return true;
                self = self.transform.parent;
            }
            
            return false;
        }
    }
}