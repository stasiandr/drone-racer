using VContainer;
using VContainer.Unity;

namespace Rings
{
    public class RingScope : LifetimeScope
    {
        public CaptureableRing ring;
        
        protected override void Configure(IContainerBuilder builder)
        {
            builder.RegisterInstance(ring).AsImplementedInterfaces();
        }
    }
}