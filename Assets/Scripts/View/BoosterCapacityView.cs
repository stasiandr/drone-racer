﻿using Player;
using TMPro;
using UnityEngine;
using VContainer;

namespace View
{
    public class BoosterCapacityView : MonoBehaviour
    {
        [SerializeField] private TMP_Text view;
        [SerializeField] private Color boostEnabledColor;
        [SerializeField] private Color boostDisabledColor;
        
        private DroneController _droneController;

        [Inject]
        public void Construct(DroneController droneController)
        {
            _droneController = droneController;
        }
        
        private void Update()
        {
            view.text = $"{_droneController.BoosterCapacity:0.0}/{_droneController.BoosterInitialCapacity:0.0}";
            view.color = _droneController.BoostEnabled ? boostEnabledColor : boostDisabledColor;
        }
    }
}